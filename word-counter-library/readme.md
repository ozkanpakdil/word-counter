# WordCounter Library

The WordCounter library is a Java-based utility that allows you to count the occurrences of words, with features such as dependency injection, concurrent counting, and exception handling.

# Features

* Dependency Injection: The library supports dependency injection to provide flexibility in configuring external components such as word validators and translators.

* ConcurrentHashMap: Word counts are stored in a thread-safe ConcurrentHashMap, making the library suitable for concurrent usage in multi-threaded environments.

* Logger Integration: Integration with SLF4J logging allows you to log word counting results and any potential errors.

* Translation Exception: The library provides a custom exception (TranslationException) specifically designed for handling errors related to word translation.

# build the library
run ./mvnw install to install the library, which will also trigger tests.