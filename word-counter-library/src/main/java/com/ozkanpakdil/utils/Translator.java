package com.ozkanpakdil.utils;

import com.ozkanpakdil.exception.TranslationException;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * External translator service.
 */
@Slf4j
public class Translator {
    private final ConcurrentHashMap<String, String> dictionary;

    /**
     * Constructor for Translator.
     *
     * @param dictionary dictionary for translation.
     */
    public Translator(ConcurrentMap<String, String> dictionary) {
        this.dictionary = (ConcurrentHashMap<String, String>) dictionary;
    }

    /**
     * Translates the given word to another language.
     *
     * @param word the word to be translated
     * @return the translated word
     */
    public String translate(String word) {
        return translate(word, "en");
    }

    /**
     * Call for the translator service.
     *
     * @param word         the word to be translated.
     * @param targetLocale the translated language.
     * @return the translated word.
     * @throws TranslationException if an error occurs during translation
     */
    private String translate(String word, String targetLocale) throws TranslationException {
        log.info("word:{}, targetLocale: {}", word, targetLocale);
        try {
            // here should be another util call to external service which may throw the exception.
            String translation = dictionary.get(word);
            if (translation == null) {
                throw new TranslationException("Searched word not found");
            }
            return translation;
        } catch (Exception e) {
            throw new TranslationException("Translation failed", e);
        }
    }
}
