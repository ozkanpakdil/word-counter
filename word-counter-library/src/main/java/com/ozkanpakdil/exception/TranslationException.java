package com.ozkanpakdil.exception;

/**
 * Translate service exception.
 */
public class TranslationException extends RuntimeException {
    /**
     * Constructor  for TranslationException.
     *
     * @param message exception message.
     * @param cause   exception cause.
     */
    public TranslationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor  for TranslationException without the cause exception.
     *
     * @param message exception message.
     */
    public TranslationException(String message) {
        super(message);
    }
}