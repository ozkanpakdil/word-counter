package com.ozkanpakdil.service;

/**
 * Word Validator
 */
public interface WordValidator {
    /**
     * Check if the word is valid.
     *
     * @param word sent word for check.
     * @return true if the word is valid otherwise false.
     */
    boolean isValidWord(String word);
}
