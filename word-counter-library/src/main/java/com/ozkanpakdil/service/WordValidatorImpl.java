package com.ozkanpakdil.service;

/**
 * Word Validator for alphanumeric chars.
 */
public class WordValidatorImpl implements WordValidator {
    private static final String ALPHANUMERIC_CHECK = "[a-zA-Z]+";

    /**
     * Constructor for WordValidatorImpl.
     */
    public WordValidatorImpl() {
        // just to prevent javadoc warning.
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValidWord(String word) {
        if (word == null || word.isEmpty()) {
            return false;
        }
        // Regex check for alphanumeric chars
        return word.trim().matches(ALPHANUMERIC_CHECK);
    }
}
