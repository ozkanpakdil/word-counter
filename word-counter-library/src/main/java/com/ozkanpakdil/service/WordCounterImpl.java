package com.ozkanpakdil.service;

import com.ozkanpakdil.exception.TranslationException;
import com.ozkanpakdil.utils.Translator;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Word counter implementation.
 */
@Slf4j
public class WordCounterImpl implements WordCounter {
    private final ConcurrentMap<String, Integer> wordCounts;
    private final WordValidator wordValidator;
    private final Translator translator;

    /**
     * Constructor for WordCounter.
     *
     * @param wordValidator word validator
     * @param translator    translator
     */
    public WordCounterImpl(WordValidator wordValidator, Translator translator) {
        this.wordValidator = wordValidator;
        this.translator = translator;
        wordCounts = new ConcurrentHashMap<>();
    }

    @Override
    public void addWords(List<String> words) throws TranslationException {
        for (String word : words) {
            if (wordValidator.isValidWord(word)) {
                try {
                    String translatedWord = translator.translate(word);
                    wordCounts.compute(translatedWord, (key, value) -> (value == null) ? 1 : value + 1);
                } catch (TranslationException e) {
                    log.error("Translation failed", e);
                    throw e;
                }
            }
        }
    }

    @Override
    public int countWord(String word) throws TranslationException {
        if (!wordValidator.isValidWord(word)) {
            return 0;
        }
        String translatedWord = translator.translate(word);
        return wordCounts.getOrDefault(translatedWord, 0);
    }

}
