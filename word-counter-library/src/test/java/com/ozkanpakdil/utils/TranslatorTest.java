package com.ozkanpakdil.utils;

import com.ozkanpakdil.exception.TranslationException;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TranslatorTest {
    Translator testee = new Translator(
            new ConcurrentHashMap<>() {
                {
                    put("flower", "flower");
                    put("flor", "flower");
                    put("blume", "flower");
                }
            }
    );

    @Test
    void translate_success() {
        assertEquals("flower", testee.translate("flower"));
        assertEquals("flower", testee.translate("flor"));
        assertEquals("flower", testee.translate("blume"));
    }

    @Test
    void translate_failure() {
        assertThrows(TranslationException.class, () -> testee.translate("flower1"));
        assertThrows(TranslationException.class, () -> testee.translate("123"));
        assertThrows(TranslationException.class, () -> testee.translate("!@#$"));
    }

}