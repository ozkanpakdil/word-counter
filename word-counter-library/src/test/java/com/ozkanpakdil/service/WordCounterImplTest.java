package com.ozkanpakdil.service;

import com.ozkanpakdil.exception.TranslationException;
import com.ozkanpakdil.utils.Translator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WordCounterImplTest {
    private WordCounterImpl testee;
    @Mock
    private WordValidatorImpl wordValidatorImpl;
    @Mock
    private Translator translator;

    @BeforeEach
    public void setUp() {
        testee = new WordCounterImpl(wordValidatorImpl, translator);
    }

    @Test
    void testAddWords_WithValidWords() throws TranslationException {
        when(translator.translate("flower")).thenReturn("flower");
        when(translator.translate("flor")).thenReturn("flower");
        when(translator.translate("blume")).thenReturn("flower");
        when(wordValidatorImpl.isValidWord("flower")).thenReturn(true);
        when(wordValidatorImpl.isValidWord("flor")).thenReturn(true);
        when(wordValidatorImpl.isValidWord("blume")).thenReturn(true);
        testee.addWords(List.of("flower", "flor", "blume"));
        assertEquals(3, testee.countWord("flower"));
    }

    @Test
    void testAddWords_WithNonAlphabeticCharacters() throws TranslationException {
        testee.addWords(List.of("flower1", "123", "!@#$"));
        assertEquals(0, testee.countWord("flower1"));
        assertEquals(0, testee.countWord("123"));
        assertEquals(0, testee.countWord("!@#$"));
    }

    @Test
    void testAddWords_WithEmptyString() throws TranslationException {
        testee.addWords(List.of(""));
        assertEquals(0, testee.countWord(""));
    }

    @Test
    void testCountWord_WithNonExistingWord() throws TranslationException {
        assertEquals(0, testee.countWord("flower"));
    }

    @Test
    void testCountWord_WithExistingWord() throws TranslationException {
        when(translator.translate("flower")).thenReturn("flower");
        when(wordValidatorImpl.isValidWord("flower")).thenReturn(true);
        testee.addWords(List.of("flower", "flower", "flower"));
        assertEquals(3, testee.countWord("flower"));
    }

    //cover the exception case for addWords method
    @Test
    void testAddWords_WithException() throws TranslationException {
        when(translator.translate("flower")).thenThrow(new TranslationException("throwing flowers"));
        when(wordValidatorImpl.isValidWord("flower")).thenReturn(true);
        List<String> flower = Collections.singletonList("flower");
        assertThrows(TranslationException.class, () -> testee.addWords(flower), "throwing flowers");
    }

    //cover the exception case for countWord method
    @Test
    void testCountWord_WithException() throws TranslationException {
        when(translator.translate("flower")).thenThrow(new TranslationException("throwing flowers"));
        when(wordValidatorImpl.isValidWord("flower")).thenReturn(true);
        assertThrows(TranslationException.class, () -> testee.countWord("flower"), "throwing flowers");
    }
}
