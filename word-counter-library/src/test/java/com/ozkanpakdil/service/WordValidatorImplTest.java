package com.ozkanpakdil.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WordValidatorImplTest {
    WordValidatorImpl testee = new WordValidatorImpl();

    @Test
    void isValidWord_success() {
        assertTrue(testee.isValidWord("flower"));
        assertTrue(testee.isValidWord("flor"));
        assertTrue(testee.isValidWord("blume"));
    }

    @Test
    void isValidWord_failure() {
        assertFalse(testee.isValidWord("flower1"));
        assertFalse(testee.isValidWord("123"));
        assertFalse(testee.isValidWord("!@#$"));
        assertFalse(testee.isValidWord(""));
        assertFalse(testee.isValidWord(null));
    }

}