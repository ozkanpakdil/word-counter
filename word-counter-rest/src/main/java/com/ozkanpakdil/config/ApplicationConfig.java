package com.ozkanpakdil.config;


import com.ozkanpakdil.service.WordCounter;
import com.ozkanpakdil.service.WordCounterImpl;
import com.ozkanpakdil.service.WordValidator;
import com.ozkanpakdil.service.WordValidatorImpl;
import com.ozkanpakdil.utils.Translator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class ApplicationConfig {

    public static final String FLOWER = "flower";

    @Bean
    public WordCounter wordCounter() {
        return new WordCounterImpl(wordValidator(), translator());
    }

    @Bean
    public Translator translator() {
        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.put(FLOWER, FLOWER);
        concurrentHashMap.put("flor", FLOWER);
        concurrentHashMap.put("blume", FLOWER);

        return new Translator(concurrentHashMap);
    }

    @Bean
    public WordValidator wordValidator() {
        return new WordValidatorImpl();
    }

}
