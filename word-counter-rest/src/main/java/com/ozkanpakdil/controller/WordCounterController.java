package com.ozkanpakdil.controller;

import com.ozkanpakdil.exception.TranslationException;
import com.ozkanpakdil.service.WordCounter;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/word-counter")
@AllArgsConstructor
public class WordCounterController {

    private final WordCounter wordCounter;

    @PostMapping("/words")
    public ResponseEntity<String> addWords(@RequestBody List<String> words) {
        try {
            wordCounter.addWords(words);
            return ResponseEntity.ok("Words added successfully");
        } catch (TranslationException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error occurred while translating words: " + e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error occurred while adding words: " + e.getMessage());
        }
    }

    @GetMapping("/count/{word}")
    public ResponseEntity<Object> countWord(@PathVariable String word) {
        try {
            int count = wordCounter.countWord(word);
            return ResponseEntity.ok(count);
        } catch (TranslationException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error occurred while translating words: " + e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(0);
        }
    }
}
