package com.ozkanpakdil.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ozkanpakdil.exception.TranslationException;
import com.ozkanpakdil.service.WordCounter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class WordCounterControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;
    @MockBean
    private WordCounter wordCounter;

    @Test
    void testCountWord_Success() throws Exception {
        String word = "flower";
        int count = 5;

        when(wordCounter.countWord(word)).thenReturn(count);

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(count)));
    }

    @Test
    void testCountWord_WithNonAlphabeticCharacters() throws Exception {
        String word = "flower1";

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void testCountWord_WithEmptyString() throws Exception {
        String word = "";

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void testCountWord_WithNonExistingWord() throws Exception {
        String word = "flower";

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void testCountWord_WithExistingWord() throws Exception {
        String word = "flower";
        int count = 5;

        when(wordCounter.countWord(word)).thenReturn(count);

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(count)));
    }

    @Test
    void testCountWord_WithTranslateException() throws Exception {
        String word = "flower";

        doThrow(new TranslationException("Error occurred")).when(wordCounter).countWord(word);

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Error occurred while translating words: Error occurred"));
    }

    @Test
    void testCountWord_WithException() throws Exception {
        String word = "flower";

        doThrow(new RuntimeException("Error occurred")).when(wordCounter).countWord(word);

        mockMvc.perform(get("/api/v1/word-counter/count/{word}", word))
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("0"));
    }

    @Test
    void testAddWords_WithNonAlphabeticCharacters() throws Exception {
        String word = "flower1";
        String words = "flower1,123,!@#$";

        mockMvc.perform(
                        post("/api/v1/word-counter/words")
                                .content(mapper.writeValueAsString(List.of(word)))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Words added successfully"));

        mockMvc.perform(
                        post("/api/v1/word-counter/words")
                                .content(mapper.writeValueAsString(List.of(words.split(","))))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Words added successfully"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "  ", "flower"})
    void testAddWords_WithEmptyString(String word) throws Exception {
        mockMvc.perform(
                        post("/api/v1/word-counter/words")
                                .content(mapper.writeValueAsString(List.of(word)))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Words added successfully"));
    }

    @Test
    void testAddWords_WithTranslationException() throws Exception {
        String word = "flower";

        doThrow(new TranslationException("Error occurred")).when(wordCounter).addWords(List.of(word));

        mockMvc.perform(
                        post("/api/v1/word-counter/words")
                                .content(mapper.writeValueAsString(List.of(word)))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Error occurred while translating words: Error occurred"));
    }

    @Test
    void testAddWords_WithException() throws Exception {
        String word = "flower";

        doThrow(new RuntimeException("Error occurred")).when(wordCounter).addWords(List.of(word));

        mockMvc.perform(
                        post("/api/v1/word-counter/words")
                                .content(mapper.writeValueAsString(List.of(word)))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Error occurred while adding words: Error occurred"));
    }

}
