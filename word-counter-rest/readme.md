# WordCounter Microservice

The WordCounter Microservice is a Java-based RESTful API that provides word counting functionality with support for multiple languages and concurrent operations.

# Features

* Word Counting: Allows users to add words and retrieve their counts, treating words in different languages as the same.

* Dependency Injection: Utilizes dependency injection for modular and flexible configuration.

* Concurrency Support: Implements a thread-safe word counting mechanism using a ConcurrentHashMap.

* Swagger UI: Provides an interactive Swagger UI for easy exploration and testing of the API endpoints.

# Test locally
**./mvnw spring-boot:run** to run the swagger ui and you can see it working here http://localhost:8080/swagger-ui/index.html

